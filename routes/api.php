<?php

use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\PostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/login', [AuthenticationController::class, 'login']);
Route::post('/register', [AuthenticationController::class, 'register']);
Route::get('/post', [PostController::class, 'index']);
    


Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('/logout', [AuthenticationController::class, 'logout']);
    Route::post('/refresh', [AuthenticationController::class, 'refresh']);
    Route::get('/me', [AuthenticationController::class, 'me']);

    Route::post('/post',[PostController::class, 'create']);
    Route::post('/post/{post}',[PostController::class, 'update']);
    Route::delete('/post/{post}',[PostController::class, 'delete']);
    Route::get('/post/{post}', [PostController::class, 'view']);

    Route::get('/post/like/{post}',[LikeController::class, 'likePost']);
    Route::get('/notifications', [NotificationController::class, 'getNotifications']);
    Route::get('/notifications/{id}', [NotificationController::class, 'markAsRead']);
});