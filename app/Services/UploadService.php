<?php

namespace App\Services;

class UploadService
{
    public const image_formats = [
        'image/jpeg',
        'image/png',
        'image/gif',
        'image/vnd.microsoft.icon',
        'image/svg+xml',
        'image/tiff',
    ];


    public static function upload($data)
    {
        $imageName = time().'.'.$data->extension();  
        $data->move(public_path('/images'), $imageName);
        return url('/images').'/'.$imageName;
    }

}