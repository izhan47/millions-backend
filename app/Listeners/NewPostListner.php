<?php

namespace App\Listeners;

use App\Events\PostEvent;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Notifications\BaseNotification;
use App\Notifications\NewPostNotification;
use Illuminate\Support\Facades\Auth;

class NewPostListner
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\PostEvent  $event
     * @return void
     */
    public function handle(PostEvent $event)
    {
        //
        $users = User::all();
        foreach($users as $user){
            if(Auth::user()->id != $user->id){
                $user->notify(new NewPostNotification($event->post));  
            }
        }
        return true;
    }
}
