<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Like extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'post_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }


    public static function checkIfExitst($post_id)
    {
        if(self::where('post_id', $post_id)->where('user_id', Auth::user()->id)->first()){
            return true;
        }
        return false;
    }

    public static function unlike($post_id)
    {
        $like = self::where('post_id', $post_id)->where('user_id', Auth::user()->id)->first();
        $like->delete();
    }

}
