<?php

namespace App\Models;

use App\Traits\UUID;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    use UUID;
    use HasFactory;

    protected $fillable = [
        'title', 'description', 'user_id', 'image'
    ];

    protected $appends = [
        'likeCount', 'liked'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function likes()
    {
        return $this->hasMany(Like::class, 'post_id');
    }

    public function post_likes()
    {
        return $this->hasMany(Like::class, 'post_id')->orderBy('created_at', 'desc')->take(5);
    }

    public function post_likes_count()
    {
        return $this->hasMany(Like::class, 'post_id')->count();
    }

    public function getLikeCountAttribute()
    {
        return $this->likes()->count();
    }

    public function getLikedAttribute()
    {
        if(Auth::user()){
            return Like::checkIfExitst($this->id);
        }
        else{
            return false;
        }
    }

    public static function userAuthorization($post_id, $user_id)
    {
        if($user_id != self::find($post_id)->user_id)
        {
            return false;
        }
        return true;
    }

    public static function deleteOldPosts()
    {
        self::where('created_at', '<=', Carbon::now()->subDays(15)->toDateTimeString())->delete();
        
    }

}
