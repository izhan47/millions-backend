<?php

namespace App\Http\Controllers;

use App\Events\PostEvent;
use App\Models\Post;
use App\Services\UploadService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;


class PostController extends Controller
{
    public function index()
    {
        $posts = Post::with('post_likes')->with('post_likes.user')->orderBy('created_at', 'desc')->paginate(10);
        return $posts;
    }

    public function view(Post $post)
    {
        $post->likes;
        foreach($post->likes as $like){
            $like->user;
        }
        return $post;
    }
    //
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'description' => 'required|string',
            'image' => 'mimes:jpeg,jpg,png'
            ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
          }
        if($request->image){
            $image = UploadService::upload($request->image);
        }else{
            $image = null;
        }
        $post = Post::create([
            'title' => $request->title,
            'description' => $request->description,
            'user_id' => Auth::user()->id,
            'image' => $image
        ]);
        event(new PostEvent($post));
        return $post;
    }

    public function update(Request $request, Post $post)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'string',
            'description' => 'string',
            'image' => 'mimes:jpeg,jpg,png'
            ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
          }
        if(!Post::userAuthorization($post->id, Auth::user()->id)){
            return response()->json(
                [
                    'message' => "Unauthorized"
                ],
                400
            ); 
        }
        if($request->image){
            $image = UploadService::upload($request->image);
            if($image['error']){
               return response()->json(
                   [
                       'message' => $image['error']
                   ],
                   400
               );
           }
           $request->image = $image['route'];
        }
        $post->update($request->all());
        return $post;
    }

    public function delete(Post $post)
    {
        if(!Post::userAuthorization($post->id, Auth::user()->id)){
            return response()->json(
                [
                    'message' => "Unauthorized"
                ],
                400
            ); 
        }
        $p=parse_url($post->image);
        if(File::exists(public_path().$p['path'])) {
            File::delete(public_path('/').$p['path']);
        }
        $post->delete();
        return response()->json(['message' => 'Post Deleted']);
    }
}
