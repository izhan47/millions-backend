<?php

namespace App\Http\Controllers;

use App\Models\Like;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{
    //
    public function likePost(Post $post)
    {
        if(Like::checkIfExitst($post->id)){
            Like::unlike($post->id);
            return response()->json(['message' => 'Unliked']);
        }else{
            Like::create([
                'user_id' => Auth::user()->id,
                'post_id' => $post->id
            ]);
            return response()->json(['message' => 'Liked']);
        }
    }
}
